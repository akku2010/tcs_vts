import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-update-cust',
  templateUrl: 'edit-dealer.html',
})
export class EditDealerPage {
  islogin: any;
  _dealerData: any;
  editDealerForm: FormGroup;
  submitAttempt: boolean;
  yearLater: any;

  constructor(
    public apiCall: ApiServiceProvider,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    navPar: NavParams,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this._dealerData = navPar.get("param");
    console.log("dealer details=> " + JSON.stringify(this._dealerData))
    if (this._dealerData.expire_date == undefined) {
      var tru = moment(new Date(), 'DD/MM/YYYY').format('YYYY-MM-DD');
      var tempdate = new Date(tru);
      tempdate.setDate(tempdate.getDate() + 365);
      this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
    } else {
      this.yearLater = moment(new Date(this._dealerData.expire_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
    }

    this.editDealerForm = formBuilder.group({
      userid: [this._dealerData.user_id, Validators.required],
      first_name: [this._dealerData.first_name, Validators.required],
      last_name: [this._dealerData.last_name, Validators.required],
      email: [(this._dealerData.email ? this._dealerData.email : 'N/A'), Validators.required],
      phone: [(this._dealerData.phone ? this._dealerData.phone : 'N/A'), Validators.required],
      address: [(this._dealerData.address ? this._dealerData.address : 'N/A'), Validators.required],
      expirationdate: [this.yearLater]
    })
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter EditDealerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  _submit() {
    this.submitAttempt = true;
    if (this.editDealerForm.valid) {
      var payload = {
        address: this.editDealerForm.value.address,
        contactid: this._dealerData._id,
        expire_date: new Date(this.editDealerForm.value.expirationdate).toISOString(),
        first_name: this.editDealerForm.value.first_name,
        last_name: this.editDealerForm.value.last_name,
        status: this._dealerData.status,
        user_id: this.editDealerForm.value.userid,
        email: this.editDealerForm.value.email
      }
      this.apiCall.startLoading().present();
      this.apiCall.editUserDetailsCall(payload)
        .subscribe(data => {
          this.apiCall.stopLoading();
          if(data) {
            let toast = this.toastCtrl.create({
              message: this.translate.instant('dealerUpdated',{value: this.translate.instant('Dealers')}),
              duration: 1500,
              position: 'middle'
            })
            toast.present();
            this.viewCtrl.dismiss();
          }

        },
          err => {
            this.apiCall.stopLoading();
            console.log("error occured=> ", err)
          });
    }
  }

}
