import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, ToastController, ModalController, PopoverController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import '../../assets/js/chartjs-plugin-labels.min.js';
import * as io from 'socket.io-client';   // with ES6 import
import { PushObject, Push, PushOptions } from '@ionic-native/push';
import { LanguagesPage } from '../login/login';
import { TranslateService } from '@ngx-translate/core';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements AfterViewInit {
  public cartCount: number = 0;

  islogin: any;
  uuid: string;
  PushToken: string;
  dealerStatus: any;
  @ViewChild('text1') text1;
  @ViewChild('chart') chart: any;
  text1Chart: any;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  @ViewChild('doughnutCanvas1') doughnutCanvas1;
  doughnutChart1: any;
  from: any;
  to: any;
  TotalDevice: any[];
  devices: any;
  Running: any;
  IDLING: any;
  Stopped: any;
  Maintance: any;
  OutOffReach: any;
  NogpsDevice: any;
  totalVechiles: any;
  devices1243: any[];
  isdevice: string;
  socket: any;
  token: string;
  totaldevices: any;
  expiredDevices: any;
  realRemindersData: any;
  progressIntervalId: any;
  loadProgress: number = 0;
  networkItervalId: any;

  constructor(
    private translate: TranslateService,
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public platform: Platform,
    private toastCtrl: ToastController,
    public push: Push,
    private popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    private geocoderApi: GeocoderProvider,
    private geolocation: Geolocation
  ) {
    this.callBaseURL();
    // this.creteSQLiteDB(); //create sqlitedb

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // this.getImgUrl();

    ///////////////////////////////
    this.events.publish('user:updated', this.islogin);
    /////////////////////////////////

    this.uuid = localStorage.getItem('uuid');
    this.PushToken = localStorage.getItem('PushToken');
    this.dealerStatus = this.islogin.isDealer;

    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();

    this.ggetMyLocation();
    this.getFuelPrice();
    console.log("smane", this.yourStateName)

    // this.events.subscribe('cart:updated', (count) => {
    //   this.cartCount = count;
    // });
    // this.events.subscribe("cart:addmore", (msg) => {
    //   debugger
    //   this.cartCount += 1;
    // })
  }

  callBaseURL() {
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }

          //// new code //
          this.checkVersion();
          this.getSocketUrl();
        });
    }
  }
  checkVersion() {
    let that = this;
    that.apiCall.getDealerInfo()
    .subscribe(response => {
        console.log("response demo data ----->",response)
        if(response.app_version) {
          this.events.publish('latest:version', response);
        }
    })
  }
  socketurl: any;
  getSocketUrl() {
    // if(localStorage.getItem("notifSocketConnected") == null){
    if (localStorage.getItem('SOCKET_URL') !== null) {
      this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
      ///////////////=======Push notifications========////////////////
      this.events.subscribe('cart:updated', (count) => {
        this.cartCount = count;
      });

      this.socket = io(this.socketurl + '/notifIOV2?userId=' + this.islogin._id, {
        transports: ['websocket', 'polling']
      });

      this.socket.on('connect', () => {
        console.log('Notification IO Connected');
        localStorage.setItem("notifSocketConnected", "YES");
        // console.log("socket connected tabs", this.socket.connected)
      });

      this.socket.on(this.islogin._id, (msg) => {
        this.cartCount += 1;
        this.events.publish("cart:addmore", msg);
      })
    }
    // }
  }

  ionViewWillEnter() {
    this.plugin();
    this.TimeMillisUtil();
    if (navigator.onLine) {
      console.log('Internet is connected');
      this.getDashboarddevices();

    } else {
      this.networkCheckInterval();
      console.log('No internet connection');
    }
  }

  ngAfterViewInit() {
    this.ggetMyLocation();
  }

  networkCheckInterval() {
    this.networkItervalId = setInterval(() => {
      if (navigator.onLine) {
        clearInterval(this.networkItervalId);
        this.getDashboarddevices();
        console.log('Internet is connected');
        this.ggetMyLocation();
      } else {
        console.log('No internet connection');
      }
    }, 2000)

  }

  ionViewDidEnter() {
    this.getApiForFuelPrices();
    localStorage.removeItem("navigationFrom");
    this.presentPopover();
    localStorage.removeItem("LiveDevice");
  }

  doRefresh(refresher) {
    console.log('executed here')
    debugger
    if ((localStorage.getItem("custumer_status") == 'OFF' || localStorage.getItem("custumer_status") == null) && (localStorage.getItem("dealer_status") == null || localStorage.getItem("dealer_status") == "OFF")) {
      this.getCustData(this.islogin._id);
    }
    this.getDashboarddevices();
    refresher.complete();
  }

  getCustData(user_id) {
    let that = this;
    that.apiCall.getcustToken(user_id).subscribe((res) => {
      debugger
      if (localStorage.getItem("pwd") !== null) {
        let passwrd = localStorage.getItem("pwd");
        if (passwrd !== res.cust.pass) {
          this.logout();
        }
      }
    },
      (err) => console.log(err))
  }
  logout() {
    if (localStorage.getItem('getDevicesInterval_ID')) {
      var intervalid = localStorage.getItem('getDevicesInterval_ID');
      clearInterval(JSON.parse(intervalid));
    }
    this.token = localStorage.getItem("DEVICE_TOKEN");
    var pushdata = {};
    if (this.platform.is('android')) {
      pushdata = {
        "uid": this.islogin._id,
        "token": this.token,
        "os": "android"
      }
    } else {
      pushdata = {
        "uid": this.islogin._id,
        "token": this.token,
        "os": "ios"
      }
    }
  }

  seeNotifications() {
    this.navCtrl.push('AllNotificationsPage');
  }
  getCounter(counter) {
    ++counter;
    if (counter < 10)
      return "000000" + counter;
    else if (counter < 100)
      return "00000" + counter;
    else if (counter < 1000)
      return "0000" + counter;
    else if (counter < 10000)
      return "000" + counter;
    else if (counter < 100000)
      return "00" + counter;
    else if (counter < 1000000)
      return "0" + counter;
    else if (counter < 10000000)
      return counter;
    else {
      counter = 1;
      return "000000" + counter;
    }
  }

  TimeMillisUtil() {
    var timMillis;//Timestamp
    var counter = 0;//Counter

    // this.getCounter(counter);
    var abc = this.getTimMillis(counter, timMillis);
    console.log("timestamp: ", abc);
  }
  getTimMillis(counter, timMillis) {
    // timMillis = System.currentTimeMillis();
    var d = new Date();
    timMillis = d.getMilliseconds();
    return this.getCounter(counter) + timMillis;
  }
  fuelCities: any = [];
  fuelTypes: any = [];
  fuels: any = [];
  getApiForFuelPrices() {
    let url = '../assets/json/fuel.json';
    this.apiCall.getSOSReportAPI(url).subscribe(data => {
      console.log("fuel prices apir response: ", data);
      this.fuelCities = data.cities;
      console.log("fuelcities", this.fuelCities);
      this.fuelTypes = data.history.datasets;
      this.fuels = data.history.datasets[0].data;
    },
      err => {
        console.log(err);
      })
  }

  fuelPricesData: any = [];
  getFuelPrice() {
    let _baseUrl = 'https://server2.oneqlik.in/googleAddress/getFuelPrice?state=' + this.yourStateName;
    this.apiCall.getSOSReportAPI(_baseUrl).subscribe(resp => {
      console.log("check fuel prices : " + resp);
      this.fuelPricesData = resp;
    },
      err => {
        console.log("chek err: ", err);
      })
  }

  yourStateName: string;
  ggetMyLocation() {
    // debugger
    this.geolocation.getCurrentPosition().then((resp) => {
      debugger
      console.log("my current location lat lng: ", resp.coords.latitude + ',' + resp.coords.longitude)
      this.geocoderApi.getStateName(resp.coords.latitude, resp.coords.longitude)
        .then(res => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          console.log("check string: ", str);
          this.yourStateName = str;
          this.getFuelPrice();
        })
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  secondSlide() {
    var role;
    if (this.islogin.isSuperAdmin === true) {
      role = 'supAdmin';
    } else if (this.islogin.isDealer === true) {
      role = 'Dealer';
    } else {
      role = 'User';
    }
    var _burl = this.apiCall.mainUrl + "gps/vehicleUtilizationPercentage?role=" + role + "&id=" + this.islogin._id;
    this.apiCall.getSOSReportAPI(_burl)
      .subscribe(resp => {
        console.log('vehicle utilization data => ', resp);
        // this.secondChart(resp);
        // this.thirdSlide();
        if (!resp.message) {
          this.secondChart(resp);
          this.thirdSlide();
        } else {
          this.thirdSlide();
          // return;
        }
      },
        err => {
          console.log(err);
        });
  }

  secondChart(bargraph) {
    if (this.barChart) {
      this.barChart.destroy();
    }

    if (bargraph.length != 0) {
      var varGraphObject = bargraph;

      for (let bg in varGraphObject) {
        console.log(bg);
        var runningArr = [
          (varGraphObject[0]['Daily']['Running'] != undefined) ? (varGraphObject[0]['Daily']['Running']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Running'] != undefined) ? (varGraphObject[1]['Weekly']['Running']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Running'] != undefined) ? (varGraphObject[2]['Monthly']['Running']).toFixed(2) : 0
        ];

        var IdleArr = [
          (varGraphObject[0]['Daily']['Idle'] != undefined) ? (varGraphObject[0]['Daily']['Idle']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Idle'] != undefined) ? (varGraphObject[1]['Weekly']['Idle']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Idle'] != undefined) ? (varGraphObject[2]['Monthly']['Idle']).toFixed(2) : 0
        ];
        var OFRArr = [
          (varGraphObject[0]['Daily']['OFR'] != undefined) ? (varGraphObject[0]['Daily']['OFR']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['OFR'] != undefined) ? (varGraphObject[1]['Weekly']['OFR']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['OFR'] != undefined) ? (varGraphObject[2]['Monthly']['OFR']).toFixed(2) : 0
        ];
        var StopArr = [
          (varGraphObject[0]['Daily']['Stop'] != undefined) ? (varGraphObject[0]['Daily']['Stop']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Stop'] != undefined) ? (varGraphObject[1]['Weekly']['Stop']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Stop'] != undefined) ? (varGraphObject[2]['Monthly']['Stop']).toFixed(2) : 0
        ];
      }

      var barChartData = {
        labels: [
          "Today",
          "Week",
          "Month"
        ],
        datasets: [
          {
            label: "Running",
            backgroundColor: "#11a46e",
            borderColor: "white",
            borderWidth: 1,
            data: [runningArr[0], runningArr[1], runningArr[2]]
          },
          {
            label: "Out of Reach",
            backgroundColor: "#1bb6d4",
            borderColor: "white",
            borderWidth: 1,
            data: [OFRArr[0], OFRArr[1], OFRArr[2]]
          },
          {
            label: "Idle",
            backgroundColor: "#ffc13c",
            borderColor: "white",
            borderWidth: 1,
            data: [IdleArr[0], IdleArr[1], IdleArr[2]]
          },
          {
            label: "Stopped",
            backgroundColor: "#ff3f32",
            borderColor: "white",
            borderWidth: 1,
            data: [StopArr[0], StopArr[1], StopArr[2]]
          }
        ]
      };

      var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          fontSize: 14,
          text: this.translate.instant('Vehicle Utilization(%)')
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 0,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },

        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: 100,
              min: 0
            }
          }]
        }
      }
    }
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: barChartData,
      options: chartOptions
    });
  }

  thirdSlide() {
    var urlB = this.apiCall.mainUrl + 'expense/dashboardExpense?id=' + this.islogin._id;
    this.apiCall.getSOSReportAPI(urlB)
      .subscribe(resp => {
        console.log("expense chart: ", resp);
        this.thirdChart(resp);
        this.fourthSlide();
      },
        err => {
          console.log('error', err);
        });
  }

  thirdChart(data) {
    if (this.doughnutChart1) {
      this.doughnutChart1.destroy();
    }
    var expenseData = data;
    var expData = [0, 0, 0, 0, 0];
    var expLevel = [this.translate.instant('Service'),
    this.translate.instant('Oil Changed'),
    this.translate.instant('Maintenance'),
    this.translate.instant('Tyres'),
    this.translate.instant('Others')]
    if (expenseData.length != 0) {
      expData = [];
      expLevel = [];
      for (var i = 0; i < expenseData.length; i++) {
        expData.push(expenseData[i].amount);
        expLevel.push(expenseData[i]._id)
      }
    }
    console.log('expDataexpDataexpDataexpDataexpData', expData);
    var daugnoutChart_1 = {
      datasets: [{
        data: expData,
        backgroundColor: ['#86AC41', '#CB0000', '#F5BE41', '#4897D8', '#5B7065'],
      }],
      labels: expLevel
    };
    this.doughnutChart1 = new Chart(this.doughnutCanvas1.nativeElement, {
      type: 'pie',
      data: daugnoutChart_1,
      options: {
        title: {
          display: true,
          fontSize: 14,
          text: this.translate.instant('Vehicle Expense')
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 0,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        tooltips: {
          enabled: true
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 10,
            fontStyle: 'bold',
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        }
      }
    });
  }
  reminers: string = 'lastSeven';
  remindersData: any = [];
  fourthSlide() {
    this.reminers = 'lastSeven';
    this.remindersData = [];
    let url = this.apiCall.mainUrl + "reminder/dashboardReminder?id=" + this.islogin._id;
    this.apiCall.getSOSReportAPI(url)
      .subscribe(respData => {
        // debugger
        console.log("get reminders=> ", respData)
        this.realRemindersData = respData;
        let lastSeven: any = [];
        lastSeven = respData[0].last_seven;
        // let lastFifteen = [];
        // lastFifteen = respData[1].last_fifteen;
        // let lastThirty = [];
        // lastThirty = respData[2].last_thirty;
        // debugger
        if (lastSeven.length > 0) {
          this.remindersData = lastSeven;
        }
      },
        err => {
          console.log("error: ", err)
        })
  }

  onReminersChange(val) {
    console.log(val)
    this.remindersData = [];
    var lastSeven = this.realRemindersData[0].seven;
    var lastFifteen = this.realRemindersData[1].fifteen;
    var lastThirty = this.realRemindersData[2].thirty;
    if (val === 'lastSeven') {
      this.remindersData = lastSeven;
    } else if (val === 'lastFifteen') {
      this.remindersData = lastFifteen;
    } else if (val === 'lastThirty') {
      this.remindersData = lastThirty;
    }
  }

  presentPopover() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (!resp.language_code) {
          let popover = this.popoverCtrl.create(LanguagesPage, {}, { enableBackdropDismiss: false });
          popover.present({});
        } else {
          if (localStorage.getItem("LanguageKey") !== null) {
            let langkey = localStorage.getItem("LanguageKey");
            if (langkey === resp.language_code) {
              this.events.publish('lang:key', resp.language_code);
            } else {
              this.events.publish('lang:key', langkey);
            }
          }
        }
      },
        err => {
          console.log(err);
        });
  }

  plugin() {
    // let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          // var width = chart.chart.width,
          //   height = chart.chart.height,
          //   ctx = chart.chart.ctx;

          // var d = Math.min(width, height);
          // var a = d / 2;

          // that.text1Chart.style.left = (((width - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.top = (((height - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.width = a + "px";
          // that.text1Chart.style.height = a + "px";
          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });
  }

  getChart() {
    let that = this;
    that.plugin();
    // if the chart is not undefined (e.g. it has been created)
    // then destory the old one so we can create a new one later
    if (this.doughnutChart) {
      this.doughnutChart.destroy();
    }

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: [this.translate.instant("RUNNING"), this.translate.instant("IDLING"), this.translate.instant("STOPPED"), this.translate.instant("OUT OF REACH"), this.translate.instant("NO GPS FIX"), this.translate.instant("EXPIRED"), this.translate.instant("NO DATA")],
        datasets: [{
          label: '# of Votes',
          data: that.TotalDevice,
          backgroundColor: [
            '#11a46e', '#ffc13c', '#ff3f32', '#1bb6d4', '#dadada', '#454344', '#7c807d'
          ],
          hoverBackgroundColor: [
            'rgba(51, 255, 153, 0.7)',
            'rgba(255, 255, 51, 0.7)',
            'rgba(255, 99, 132, 0.7)',
            'rgba(204, 204, 179, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(255, 102, 51, 0.7)',
            'rgba(0,0,0,0.5)',
            'rgba(167, 171, 168,0.7)'
          ]
        }]
      },
      options: {
        cutoutPercentage: 70,
        title: {
          display: true,
          fontSize: 14,
          text: this.translate.instant('Vehicle Current Status')
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 16,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        elements: {
          center: {
            text: that.totaldevices,
            color: '#197197', // Default is #000000
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 10, // Defualt is 20 (as a percentage),
          },

        },
        tooltips: {
          enabled: false
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 10,
            fontStyle: 'bold',
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        },
        onClick: function (event, elementsAtEvent) {
          var activePoints = elementsAtEvent;
          if (activePoints[0]) {
            // debugger
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];
            var label = chartData.labels[idx];
            var value = chartData.datasets[0].data[idx];
            console.log("value dashboard: ", value)
            if (label === that.translate.instant('EXPIRED')) {
              localStorage.setItem('status', 'Expired');
            } else if (label === that.translate.instant('NO GPS FIX')) {
              localStorage.setItem('status', 'NO GPS FIX');
            } else if (label === that.translate.instant('RUNNING')) {
              localStorage.setItem('status', 'RUNNING');
            } else if (label === that.translate.instant('IDLING')) {
              localStorage.setItem('status', 'IDLING');
            } else if (label === that.translate.instant('STOPPED')) {
              localStorage.setItem('status', 'STOPPED');
            } else if (label === that.translate.instant('OUT OF REACH')) {
              localStorage.setItem('status', 'OUT OF REACH');
            } else if (label === that.translate.instant('NO DATA')) {
              localStorage.setItem('status', 'NO DATA');
            }
            that.navCtrl.push('AddDevicesPage', {
              label: label,
              value: value
            });
          }
        }
      }
    });
  }

  historyDevice() {
    // localStorage.setItem("MainHistory", "MainHistory");
    this.navCtrl.push('HistoryDevicePage');
  }

  opennotify() {
    this.navCtrl.push('AllNotificationsPage')
  }

  addPushNotify() {
    console.log("VeryFirstLoginUser: ", localStorage.getItem("VeryFirstLoginUser"));
    let that = this;
    that.token = "";
    var pushdata;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    var v_id = localStorage.getItem("VeryFirstLoginUser");
    if (that.token == null) {
      this.pushSetup();
    } else {
      if (this.platform.is('android')) {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "ios"
        }
      }

      if (v_id == that.islogin._id) {
        that.apiCall.pushnotifyCall(pushdata)
          .subscribe(data => {
            console.log("push notifications updated " + data.message)
          },
            error => {
              console.log(error);
            });
      }
      else {
        console.log("Token already updated!!")
      }
    }
  }

  pushSetup() {
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('Channel created'));
    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "notif",
      description: "default description",
      sound: 'notif',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('sos Channel created'));
    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("dashboard entered device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  progressBar() {
    // Test interval to show the progress bar
    this.progressIntervalId = setInterval(() => {
      if (this.loadProgress < 100) {
        this.loadProgress += 1;

        // console.log("progress: ", this.loadProgress);
      }
      else {
        // if(that.totaldevices)
        clearInterval(this.loadProgress);
      }
    }, 100);
  }

  getDashboarddevices() {
    console.log("get Dashboard devices");

    var _baseUrl = this.apiCall.mainUrl + 'gps/getDashboard?email=' + this.islogin.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.islogin._id;
    if (this.islogin.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        _baseUrl += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    this.apiCall.dashboardcall(_baseUrl)
      .subscribe(data => {
        let that = this;
        that.loadProgress = 100;
        clearInterval(that.progressIntervalId);

        this.totaldevices = data.Total_Vech;
        localStorage.setItem('Total_Vech', data.Total_Vech)
        // this.creteSQLiteDB();
        ///// add push token to server //////////////
        this.addPushNotify();
        /////////////////////////////////////////////

        this.TotalDevice = [];
        this.devices = data;
        this.Running = this.devices.running_devices;
        this.TotalDevice.push(this.Running)
        this.IDLING = this.devices["Ideal Devices"]
        this.TotalDevice.push(this.IDLING);
        this.Stopped = this.devices["OFF Devices"]
        this.TotalDevice.push(this.Stopped);
        // this.Maintance = this.devices["Maintance Device"]
        // this.TotalDevice.push(this.Maintance);
        this.OutOffReach = this.devices["OutOfReach"]
        this.TotalDevice.push(this.OutOffReach);
        this.NogpsDevice = this.devices["no_gps_fix_devices"]
        this.TotalDevice.push(this.NogpsDevice);
        this.expiredDevices = this.devices["expire_status"];
        this.TotalDevice.push(this.expiredDevices);
        this.TotalDevice.push(this.devices["no_data"]);
        this.totalVechiles = this.Running + this.IDLING + this.Stopped + this.OutOffReach + this.NogpsDevice + this.expiredDevices + this.devices["no_data"];
        this.getChart();
        this.secondSlide();


      },
        err => {
          console.log(err);
          let that = this;
          clearInterval(that.progressIntervalId);
        });
  }

  vehiclelist() {
    console.log("coming soon");
    this.navCtrl.push('AddDevicesPage');
  }

  live() {
    if (this.platform.is('ios')) {
      this.navCtrl.push('LivePage', {}, { animate: true, direction: 'back' });
    } else {
      this.navCtrl.push('LivePage')
    }
  }

  geofence() {
    this.navCtrl.push('GeofencePage');
  }

}
