import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';


@IonicPage()
@Component({
  selector: 'page-working-hours-report',
  templateUrl: 'working-hours-report.html',
})
export class WorkingHoursReportPage {

  islogin: any;
  Ignitiondevice_id: any = [];
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  workingHours: any[] = [];
  workingReport: any = [];
  workingReportData: any = [];
  daywiseReport: any = [];
  a: number;
  b: number;
  c: number;
  value: number;
  TotalWorkingHours: string = "";
  device_id: any = []
  locationEndAddress: any;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  test: any;
  today_stop: any = [];
  hours: any
  resp_data: any;
  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public toastCtrl: ToastController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }
  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.igiReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format();
      this.datetimeEnd = moment({ hours: 0 }).format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getIgnitiondevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    // this.Ignitiondevice_id = selectedVehicle.Device_Name;

    this.Ignitiondevice_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.Ignitiondevice_id.push(selectedVehicle[t].Device_ID)
        }
      } else {
        this.Ignitiondevice_id.push(selectedVehicle[0].Device_ID)
      }
    } else return;
    console.log("selectedVehicle=> ", this.Ignitiondevice_id)
  }



  wrReport = []
  getIgnitiondeviceReport() {
    this.TotalWorkingHours = "";
    this.workingReport = [];
    this.wrReport = [];
    if (this.Ignitiondevice_id.length === 0) {
      this.Ignitiondevice_id = [];

    }
    let that = this;
    this.apicalligi.startLoading().present();
    var b_url = this.apicalligi.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apicalligi.urlpasseswithdata(b_url, Var).subscribe(
      (resp) => {
        console.log("respppppppppppp-------->", resp)
        console.log("resp----data========>", resp.digital_input)
        this.resp_data = resp;
        if (this.resp_data.digital_input == 1) {
          debugger
          let startDate = new Date(this.datetimeStart);
          let endDate = new Date(this.datetimeEnd);
          this.apicalligi.getnotifsignitionReportForMobile(startDate.toISOString(),
            endDate.toISOString(), this.islogin._id, this.Ignitiondevice_id)
            .subscribe(data => {
              console.log("data workingggg====>", data)
              debugger
              this.workingHours = data;
              console.log("igi", data);

              this.apicalligi.stopLoading();
              this.workingReport = data;
              //console.log("mili", this.workingReport[r]['ON-OFF_time']);
              debugger;
              let TotalWorkingHours = 0;
              for (var r in data) {
                // console.log('foreach',resp[r]);
                var tempObj = {
                  'vehName': r,
                  'ON-OFF_time': this.workingReport[r]['ON-OFF_time'] ? this.millisecondConversion(this.workingReport[r]['ON-OFF_time']) : 0,
                  'OFF-ON_time': this.workingReport[r]['OFF-ON_time'] ? this.millisecondConversion(this.workingReport[r]['OFF-ON_time']) : 0,
                  'stoppagetime': calculateStoppageTime(this.workingReport[r]['ON-OFF_time'])
                  //'duration' : this.millisecondConversion(this.workingReport[r]['ON-ON-OFF_time'])
                }
                this.wrReport.push(tempObj);
                let a = tempObj['ON-OFF_time'].toString().split(':')
                TotalWorkingHours += Number(a[0]) * 60 + (Number(a[1]) ? Number(a[1]) : 0);
              }



              function calculateStoppageTime(wh) {
                debugger
                var totalwr = new Date(that.datetimeEnd).getTime() - new Date(that.datetimeStart).getTime();
                console.log("totalstopaage");
                var shr = totalwr - wh;
                console.log("shr", shr);
                // var convert = that.millisecondConversion(shr);
                // console.log("finalConvertTime", convert);
                return that.millisecondConversion(shr);
              }

              this.TotalWorkingHours = Math.trunc(TotalWorkingHours / 60) + ":" + TotalWorkingHours % 60;      //this.innerFunc(this.workingReport);
              console.log("totalhours", this.TotalWorkingHours);


            }, error => {
              this.apicalligi.stopLoading();
              console.log(error);
            })
        } else if (this.resp_data.digital_input == 2) {
          let startDate = new Date(this.datetimeStart);
          let endDate = new Date(this.datetimeEnd);
          this.apicalligi
            .getnotifsacReportForMobile(
              startDate.toISOString(),
              endDate.toISOString(),
              this.islogin._id,
              this.Ignitiondevice_id,
            )
            .subscribe(
              (data) => {
                this.workingHours = data;
                // console.log('ddddddddddddddddddddd------------->',data);
              },
              (error) => {
                console.log("error in getdistancespeed =>  ", error);
              }
            );
        }
        console.log("check lang key: ", resp);
   
      }
    );

  }




  innerFunc(workingReport) {
    for (var r in workingReport) {
      var tempObj = {
        'vehName': r,
        'ontime': this.millisecondConversion(workingReport[r]['ON-OFF_time']),
        'offtime': this.millisecondConversion(workingReport[r]['OFF-ON_time'])
      }
      this.workingReportData.push(tempObj);


    }
    console.log("push data", this.workingReportData);
  }
  millisecondConversion(duration) {
    var seconds = Math.floor((duration / 1000) % 60);
    var minutes: any = Math.floor((duration / (1000 * 60)) % 60);
    var hours: any = Math.floor((duration / (1000 * 60 * 60)));

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? 0 + seconds : seconds;
    return hours + ":" + minutes;
  }



  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }



  timeToMins(time) {
    var b = time.split(':');
    return b[0] * 60 + +b[1];
  }

  // Convert minutes to a time in format hh:mm
  // Returned value is in range 00  to 24 hrs
  timeFromMins(mins) {
    function z(n) { return (n < 10 ? '0' : '') + n; }
    var h = (mins / 60 | 0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
  }

  // Add two times in hh:mm format
  addTimes(a, b) {
    return this.timeFromMins(this.timeToMins(this.a) + this.timeToMins(this.b));
  }

}
