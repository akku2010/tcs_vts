import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { AppLanguages } from '../providers/app-languages';
import { StatusBar } from '@ionic-native/status-bar';
import {SuccessDetailPage} from '../pages/add-devices/paytmwalletlogin/paytmwalletlogin'
import {PaymentHistoryPage} from '../pages/add-devices/paytmwalletlogin/paytmwalletlogin'
import {PaymantgatweyPage} from '../pages/add-devices/add-devices'
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { CustomDurationPage, PopoverPage } from '../pages/add-devices/add-devices';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { Push } from '@ionic-native/push';
import { UpdateGroup } from '../pages/groups/update-group/update-group';
import { FilterPage } from '../pages/all-notifications/filter/filter';
import { AndroidPermissions } from '@ionic-native/android-permissions';
// Custom components
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { ServiceProviderPage, UpdatePasswordPage } from '../pages/profile/profile';
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { PoiPage } from '../pages/live-single-device/live-single-device';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ViewDoc } from '../pages/add-devices/upload-doc/view-doc/view-doc';
import { DocPopoverPage } from '../pages/add-devices/upload-doc/upload-doc';
import { ACDetailPage } from '../pages/ac-report/ac-report';
import { FuelEntryPage } from '../pages/fuel/fuel-consumption/fuel-consumption';
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LanguagesPage } from '../pages/login/login';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { PagerProvider } from '../providers/pager/pager';
import { ModalPage } from '../pages/history-device/modal';
import { NotifModalPage } from '../pages/profile/settings/notif-setting/notif-modal';
import { ReportSettingModal } from '../pages/customers/modals/report-setting/report-setting';
import { FasttagPayNow } from '../pages/fastag-list/fastag/fastag';
import { DealerPermModalPage } from '../pages/dealers/dealer-perm/dealer-perm';
import { SMS } from '@ionic-native/sms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DetailedPage } from '../pages/supported-devices/detailed/detailed';
import { ImmobilizeModelPage } from '../pages/add-devices/immobilize-modal';
import { AddPointsModalPage } from '../pages/dealers/AddPointsModalPage';
import { ChatService } from '../providers/chat-service';
import { TimePickerModal } from '../pages/profile/settings/notif-setting/time-picker/time-picker';
import { DatePipe } from '@angular/common';
import { NativeAudio } from '@ionic-native/native-audio';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { NearbyVehicleModal } from '../pages/live-single-device/neraby-vehicle-modal/neraby-vehicle-modal';
import { MsgUtilityPage } from '../pages/msg-utility/msg-utility';
import { DaywiseDistancePage } from '../pages/daywise-distance/daywise-distance';
import { MsgUtilityPageModule } from '../pages/msg-utility/msg-utility.module';
import { Insomnia } from '@ionic-native/insomnia';
import { DaywiseDistancePageModule } from '../pages/daywise-distance/daywise-distance.module';
import { AlertModelPage } from '../pages/dashboard/alertModelPage';
import { AnnounceModelPage } from '../pages/dashboard/announceModel';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ShowSupportContactsModal } from '../pages/contact-us/contact-us';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { Clipboard } from '@ionic-native/clipboard';
import {ParkingAlarmPage} from '../pages/profile/settings/notif-setting/time-picker/time-picker'
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
// export function createTranslateLoader(http: Http) {
//   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
// }

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    PopoverPage,
    UpdateGroup,
    // OnCreate,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    LanguagesPage,
    SuccessDetailPage,
    PaymentHistoryPage,
    PaymantgatweyPage,
    ModalPage,
    NotifModalPage,
    ReportSettingModal,
    FasttagPayNow,
    DealerPermModalPage,
    AlertModelPage,
    AnnounceModelPage,
    DetailedPage,
    ImmobilizeModelPage,
    //DaywiseDistancePage,
    // MsgUtilityPage,
    AddPointsModalPage,
    TimePickerModal,
    ParkingAlarmPage,
    // AddBookingModal,
    NearbyVehicleModal,
    CustomDurationPage,
    ShowSupportContactsModal
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    MsgUtilityPageModule,
    DaywiseDistancePageModule,
    IonicModule.forRoot(MyApp, {
      // scrollPadding: false,
      // scrollAssist: true,
      // autoFocusAssist: true
      // scrollAssist: false,
      // autoFocusAssist: false
    }),
    // SignaturePadModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      }
    }),
    IonBottomDrawerModule,
    SelectSearchableModule,
    // PDFGeneratorModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverPage,
    UpdateGroup,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    SuccessDetailPage,
    PaymentHistoryPage,
    PaymantgatweyPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    LanguagesPage,
    ModalPage,
    NotifModalPage,
    AlertModelPage,
    AnnounceModelPage,
    ReportSettingModal,
    FasttagPayNow,
    DealerPermModalPage,
    DetailedPage,
    ImmobilizeModelPage,
    DaywiseDistancePage,
    MsgUtilityPage,
    AddPointsModalPage,
    TimePickerModal,
    ParkingAlarmPage,
    // AddBookingModal,
    NearbyVehicleModal,
    CustomDurationPage,
    ShowSupportContactsModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicErrorHandler,
    // NativePageTransitions,
    // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    GoogleMaps,
    Spherical,
    Push,
    AppVersion,
    SocialSharing,
    TextToSpeech,
    Transfer,
    TransferObject,
    File,
    FileTransfer,
    FileTransferObject,
    Camera,
    FilePath,
    NativeGeocoder,
    ScreenOrientation,
    AppLanguages,
    GeocoderProvider,
    GeocoderProvider,
    PagerProvider,
    AndroidPermissions,
    SMS,
    BarcodeScanner,
    Insomnia,
    SpeechRecognition,
    ChatService,
    DatePipe,
    NativeAudio,
    Geolocation,
    InAppBrowser,
    CallNumber,
    ThemeableBrowser,
    Clipboard
  ]
})
export class AppModule { }
